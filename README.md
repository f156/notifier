# Notification

Микросервис для отправки уведомлений из шаблона.

API для хранения шаблонов и отправка (email, push)

```yml
notification:
  build:
    context: ../notification
    dockerfile: Dockerfile
  image: notification:latest
  entrypoint: /notification/docker-compose-entrypoint.sh
  command: [ 'rails', 'server', '-b', '0.0.0.0', '-p', '3000' ]
  ports:
    - 4040:3000
  volumes:
    - ../notification:/notification
  stdin_open: true
  tty: true
  environment:
    - "RAILS_LOG_TO_STDOUT=true"
    - "RAILS_ENV=development"
  depends_on:
    - postgres
```

```yml
  notification-sidekiq:
    build:
      context: ../notification
      dockerfile: Dockerfile
    image: notification:latest
    entrypoint: /notification/docker-compose-entrypoint.sh
    command: bundle exec sidekiq
    volumes:
      - ../notification:/notification
    stdin_open: true
    tty: true
    environment:
      - "RAILS_LOG_TO_STDOUT=true"
      - "RAILS_ENV=development"
    depends_on:
      - postgres
      - redis
```

## Example of using actioncable on frontend

```javascript
import ActionCable from 'actioncable';

const actionCable = ActionCable.createConsumer('ws://notification:4040/cable');

console.log('actionCable:', actionCable)

const user_uid = '792a09e0-dd63-4a3a-a45e-b9ab87e0c148'

actionCable.subscriptions.create('TestChannel', {
  received(data) {
    console.log('Received data:', data)
    alert(data.current_time)
  },
  disconnected() {
    console.log('disconnected')
  },
  connected() {
    console.log('connected')
  }
})

actionCable.subscriptions.create('UserChannel', {
  received(data) {
    console.log('Received data:', data)
    alert(data.text)
  },
  disconnected() {
    console.log('disconnected')
  },
  connected() {
    console.log('connected')
    this.notify(user_uid);
  },
  notify(user_uid) {
    this.perform('notify', { user_uid: user_uid })
  }
})

function App() {
  return (
    'hello'
  );
}

export default App;

```
