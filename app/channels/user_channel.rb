class UserChannel < ApplicationCable::Channel
  def notify(params)
    stop_all_streams
    stream_from "user_channel/#{params['user_uid']}"
  end
end
