class Api::V1::NotificationsController < ApplicationController
  include RequredParams

  set_pagination_headers :notifications, only: [:index]

  def index
    @notifications = by_user_uid(Notification.paginate(page: params[:page], per_page: params[:per_page]))

    render_success(@notifications)
  end

  def create
    service = Notification::Create.call(notification_params)

    render_success(service.notification, :created)
  end

  def update
    notification = Notification.find(params[:id])

    notification.update!(update_notification_params)
  end

  def count
    render json: by_user_uid(Notification).count
  end

  private

  def notification_params
    params.require(:notification).permit(:way, :user_uid, :title, :text, :template_code, template_params: {})
  end

  def update_notification_params
    params.require(:notification).permit(:is_readed)
  end

  def serializer
    NotificationSerializer
  end

  def by_user_uid(scope)
    return scope.where(user_uid: params[:user_uid]) if params[:user_uid].present?

    scope
  end

  def requred_params
    {
      keys: %w[way user_uid],
      params: notification_params
    }
  end
end
