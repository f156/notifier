class Api::V1::TemplatesController < ApplicationController
  include RequredParams

  before_action :load_resourse, only: %i[show update destroy]

  set_pagination_headers :scope, only: [:index]

  def index
    @scope = Template.paginate(page: params[:page], per_page: params[:per_page])

    @scope = @scope.where(code: params[:code]) if params[:code].present?

    render_success(@scope)
  end

  def show
    render_success(@resourse)
  end

  def update
    @resourse.update!(template_params)

    render_success(@resourse)
  end

  def create
    render_success(Template.create!(template_params), :created)
  end

  def destroy
    @resourse.destroy
  end

  def count
    render json: Template.count
  end

  private

  def load_resourse
    @resourse = Template.find(params[:id])
  end

  def template_params
    params.require(:template).permit(:code, :title, :text)
  end

  def serializer
    TemplateSerializer
  end

  def requred_params
    {
      keys: %w[code title text],
      params: template_params
    }
  end
end
