class ApplicationController < ActionController::API
  rescue_from StandardError do |e|
    render_errors(:standard_error, 500, e, '00')
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    render_errors(:record_not_found, 404, e, '01')
  end

  rescue_from ActiveModel::RangeError do |e|
    render_errors(:record_not_found, 404, e, '01')
  end

  rescue_from ActiveRecord::RecordInvalid do |e|
    render_errors(:record_invalid, 422, e, '02')
  end

  rescue_from ActionController::ParameterMissing do |e|
    render_errors(:missing_parameters, :bad_request, e, '03')
  end

  rescue_from CustomError do |e|
    render_errors(e.error, e.status, e.message.to_s, e.error_code)
  end

  def route_not_found
    render_errors(:routing_error, 404, "path not found: #{params[:unmatched]}", '04')
  end

  private

  def render_errors(error, status, exception, error_code)
    Rails.logger.error(exception.full_message) if Rails.env.development? && exception.respond_to?(:full_message)

    json = { error: error, message: exception, code: "#{Rails.configuration.error_code_prefix}#{error_code}" }

    render json: json, status: status
  end

  def render_success(obj, status = :ok)
    raise ActiveRecord::RecordNotFound, 'Record not found' if obj.nil?

    render json: obj, status: status, each_serializer: serializer
  end

  class << self
    def set_pagination_headers(name, options = {})
      after_action(options) do
        results = instance_variable_get("@#{name}")
        headers['X-Pagination-Total-Count'] = results.total_entries
        headers['X-Pagination-Page-Count'] = results.total_pages
      end
    end
  end
end
