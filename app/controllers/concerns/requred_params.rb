module RequredParams
  extend ActiveSupport::Concern

  included do
    before_action :check_params, only: [:create]
  end

  def create; end

  private

  def requred_params
    {
      keys: %w[],
      params: {}
    }
  end

  def check_params
    invalid_params = requred_params[:keys].reject { |key| requred_params[:params].keys.include?(key) }

    return if invalid_params.empty?

    raise ActionController::ParameterMissing, invalid_params.join(', ')
  end
end
