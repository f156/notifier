class TestActioanCablesController < ApplicationController
  def show
    ActionCable.server.broadcast 'test_channel', { current_time: Time.current }

    render json: { current_time: Time.current }
  end
end
