class CustomError < StandardError
  attr_reader :status, :error, :message, :error_code

  def initialize(error, status, message, error_code = nil)
    super(message)

    @error = error || 422
    @status = status || :unprocessable_entity
    @message = message || 'Something went wrong'
    @error_code = error_code
  end

  def fetch_json
    { error: error, message: message, status: status, error_code: error_code }
  end
end
