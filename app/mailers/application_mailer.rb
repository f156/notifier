class ApplicationMailer < ActionMailer::Base
  default from: Rails.configuration.info_email
  layout 'mailer'
end
