class NotificationMailer < ApplicationMailer
  def call(notification_id)
    notification = Notification.find(notification_id)

    @text = notification.text.html_safe

    mail(to: notification.location, subject: notification.title)
  end
end
