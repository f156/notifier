# == Schema Information
#
# Table name: notifications
#
#  id            :uuid             not null, primary key
#  way           :integer          not null
#  user_uid      :uuid             not null
#  title         :string
#  text          :text
#  location      :string
#  template_code :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  is_readed     :boolean          default: false
#

class Notification < ApplicationRecord
  validates :way, :user_uid, presence: true

  validates :title, :text, presence: true, if: -> { template_code.nil? }

  belongs_to :template,
             foreign_key: 'template_code',
             primary_key: 'code',
             optional: true,
             inverse_of: false

  enum way: { email: 0, sms: 1, push: 2 }
end
