# == Schema Information
#
# Table name: templates
#
#  id         :uuid             not null, primary key
#  code       :string           not null
#  title      :string           not null
#  text       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Template < ApplicationRecord
  validates :code, :title, :text, presence: true

  validates :code, uniqueness: true
end
