# == Schema Information
#
# Table name: notifications
#
#  id            :uuid             not null, primary key
#  way           :integer          not null
#  user_uid      :uuid             not null
#  title         :string
#  text          :text
#  location      :string
#  template_code :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class NotificationSerializer < ActiveModel::Serializer
  include Shared::UnixTimestamp

  attributes :id, :way, :user_uid, :title, :text, :location, :template_code, :created_at, :updated_at, :is_readed
end
