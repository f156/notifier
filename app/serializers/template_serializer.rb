# == Schema Information
#
# Table name: templates
#
#  id         :uuid             not null, primary key
#  code       :string           not null
#  title      :string           not null
#  text       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TemplateSerializer < ActiveModel::Serializer
  include Shared::UnixTimestamp

  attributes :id, :code, :title, :text, :created_at, :updated_at
end
