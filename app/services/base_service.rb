class BaseService
  def self.call(*args)
    object = new(*args)
    object.call
    object
  end

  def initialize(params)
    @params = params
  end

  def call; end
end
