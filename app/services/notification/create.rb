class Notification::Create < BaseService
  attr_reader :notification, :params

  def call
    prepared_location
    prepared_text
    @notification = Notification.create!(params)
    Notification::Execute.call(@notification)
  end

  private

  def prepared_location
    params.merge!(location: location)
  end

  def location
    begin
      user = KwalletModels::User.find(params[:user_uid]).user
    rescue StandardError
      raise UserNotFound.new('user_not_found', 404, 'User not found by user_uid', '06')
    end

    {
      email: user.email,
      sms: (user.phone if user.respond_to?(:phone))
    }[params[:way].to_sym]
  end

  def prepared_text
    return if params[:template_code].blank? || template.nil?

    replace_text

    params.merge!(text: template.text, title: template.title)
  end

  def replace_text
    params[:template_params].each { |k, v| template.text.gsub!("{{#{k}}}", v) }
    params.delete(:template_params)
  end

  def template
    @template ||= Template.find_by(code: params[:template_code])
  end
end
