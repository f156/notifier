class  Notification::Execute
  def initialize(notification)
    @notification = notification
  end

  def call
    case @notification.way.to_sym
    when :email
      NotificationMailer.call(@notification.id).deliver_later
    when :sms
      SmsService.call
    when :push
      ActionCable.server.broadcast("user_channel/#{@notification.user_uid}", @notification)
    end
  end
end
