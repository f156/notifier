redis = {
  url: Rails.application.config.redis_url,
  password: Rails.application.config.redis_pass
}

Sidekiq.configure_server { |config| config.redis = redis }
Sidekiq.configure_client { |config| config.redis = redis }
