Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  root to: proc { [200, {}, ['ok']] }

  get 'favicon.ico', to: proc { [200, {}, []] }

  resource :test_actioan_cable, only: [:show]

  namespace :api do
    namespace :v1 do
      resources :notifications, only: %i[index create update] do
        collection do
          get :count
        end
      end

      resources :templates do
        collection do
          get :count
        end
      end
    end
  end
end
