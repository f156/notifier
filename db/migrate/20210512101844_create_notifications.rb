class CreateNotifications < ActiveRecord::Migration[6.1]
  enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')

  def change
    create_table :notifications, id: :uuid do |t|
      t.integer :way, null: false
      t.uuid :user_uid, null: false
      t.string :title
      t.text :text
      t.string :location
      t.string :template_code

      t.timestamps null: false
    end
  end
end
