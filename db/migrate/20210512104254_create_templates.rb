class CreateTemplates < ActiveRecord::Migration[6.1]
  enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')

  def change
    create_table :templates, id: :uuid do |t|
      t.string :code, null: false
      t.string :title, null: false
      t.text :text, null: false

      t.timestamps null: false
    end

    add_index :templates, :code, unique: true
  end
end
