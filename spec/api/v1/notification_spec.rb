require 'rails_helper'

describe 'Notification API' do
  describe 'GET /index' do
    context 'without pagination' do
      let!(:notifications) { create_list(:notification, 12) }

      before { get '/api/v1/notifications' }

      it { expect(response.successful?).to be true }
      it { expect(response.body).to have_json_size(10) }

      %w[id way user_uid title text location template_code].each do |attr|
        it { expect(response.body).to be_json_eql(notifications.first.send(attr).to_json).at_path("0/#{attr}") }
      end

      %w[created_at updated_at].each do |attr|
        it { expect(response.body).to be_json_eql(notifications.first.send(attr).to_i.to_json).at_path("0/#{attr}") }
      end
    end

    context 'with pagination' do
      let!(:notifications) { create_list(:notification, 8) }

      before do
        get '/api/v1/notifications',
            params: {
              page: 2,
              per_page: 5
            }
      end

      it { expect(response.successful?).to be true }
      it { expect(response.body).to have_json_size(3) }
    end

    context 'with user_uid' do
      let!(:notifications) { create_list(:notification, 2) }

      before do
        get '/api/v1/notifications', params: { user_uid: notifications.first.user_uid }
      end

      it { expect(response.successful?).to be true }
      it { expect(response.body).to have_json_size(1) }

      %w[id way user_uid title text location template_code].each do |attr|
        it { expect(response.body).to be_json_eql(notifications.first.send(attr).to_json).at_path("0/#{attr}") }
      end
    end
  end

  describe 'GET /count' do
    let!(:count) { 3 }
    let!(:notifications) { create_list(:notification, count) }

    context 'without user_uid' do
      before { get '/api/v1/notifications/count' }

      it { expect(response.successful?).to be true }
      it { expect(response.body).to eq count.to_s }
    end

    context 'with user_uid' do
      before { get '/api/v1/notifications/count', params: { user_uid: notifications.first.user_uid } }

      it { expect(response.successful?).to be true }
      it { expect(response.body).to eq '1' }
    end
  end

  describe 'POST /create' do
    let(:user_uid) { '8d95a7b5-08f1-4c3a-8efd-e0069e791c00' }

    before do
      profile_host = Rails.configuration.profile_host_url
      profile_path = "api/v1/users/#{user_uid}.json"
      url = "#{profile_host}/#{profile_path}"

      stub_request(:get, url).to_return(status: 200, body: create(:profile).user.to_h.to_json)
    end

    context 'without template_code' do
      let!(:notification_params) do
        {
          way: 'email',
          title: 'title',
          text: 'text',
          user_uid: user_uid
        }
      end

      before { post '/api/v1/notifications', params: { format: :json, notification: notification_params } }

      it { expect(response.successful?).to be true }
      it { expect(response.code).to eq '201' }

      %i[way user_uid title text template_code].each do |attr|
        it { expect(response.body).to be_json_eql(notification_params[attr].to_json).at_path(attr.to_s) }
      end

      it {
        expect do
          post '/api/v1/notifications', params: { format: :json, notification: notification_params }
        end.to change(Notification, :count).by(1)
      }
    end

    context 'with template_code' do
      let!(:template) { create(:template, text: 'var1 = {{var1}}, var2 = {{var2}}, var1 = {{var1}}') }

      let!(:notification_params) do
        {
          way: 'email',
          user_uid: user_uid,
          template_code: template.code,
          template_params: { var1: 'foo1', var2: 'foo2' }
        }
      end

      before { post '/api/v1/notifications', params: { format: :json, notification: notification_params } }

      it { expect(response.successful?).to be true }
      it { expect(response.code).to eq '201' }

      %i[way user_uid template_code].each do |attr|
        it { expect(response.body).to be_json_eql(notification_params[attr].to_json).at_path(attr.to_s) }
      end

      it { expect(response.body).to be_json_eql(template.title.to_json).at_path('title') }
      it { expect(response.body).to be_json_eql('var1 = foo1, var2 = foo2, var1 = foo1'.to_json).at_path('text') }

      it {
        expect do
          post '/api/v1/notifications', params: { format: :json, notification: notification_params }
        end.to change(Notification, :count).by(1)
      }
    end
  end
end
