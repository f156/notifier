require 'rails_helper'

describe 'Template API' do
  describe 'GET /index' do
    context 'without pagination' do
      let!(:templates) { create_list(:template, 12) }

      before { get '/api/v1/templates' }

      it { expect(response.successful?).to be true }
      it { expect(response.body).to have_json_size(10) }

      %w[id code title text].each do |attr|
        it { expect(response.body).to be_json_eql(templates.first.send(attr).to_json).at_path("0/#{attr}") }
      end

      %w[created_at updated_at].each do |attr|
        it { expect(response.body).to be_json_eql(templates.first.send(attr).to_i.to_json).at_path("0/#{attr}") }
      end
    end

    context 'with pagination' do
      let!(:templates) { create_list(:template, 8) }

      before do
        get '/api/v1/templates',
            params: {
              page: 2,
              per_page: 5
            }
      end

      it { expect(response.successful?).to be true }
      it { expect(response.body).to have_json_size(3) }
    end

    context 'with code' do
      let!(:templates) { create_list(:template, 2) }

      before do
        get '/api/v1/templates', params: { code: templates.first.code }
      end

      it { expect(response.successful?).to be true }
      it { expect(response.body).to have_json_size(1) }

      %w[id code title text].each do |attr|
        it { expect(response.body).to be_json_eql(templates.first.send(attr).to_json).at_path("0/#{attr}") }
      end
    end
  end

  describe 'GET /count' do
    let!(:count) { 3 }
    let!(:templates) { create_list(:template, count) }

    before { get '/api/v1/templates/count' }

    it { expect(response.successful?).to be true }
    it { expect(response.body).to eq count.to_s }
  end

  describe 'POST /create' do
    let!(:template_params) do
      {
        code: 'code',
        title: 'title',
        text: 'text'
      }
    end

    before { post '/api/v1/templates', params: { format: :json, template: template_params } }

    it { expect(response.successful?).to be true }
    it { expect(response.code).to eq '201' }

    %i[code title text].each do |attr|
      it { expect(response.body).to be_json_eql(template_params[attr].to_json).at_path(attr.to_s) }
    end

    it {
      expect do
        post '/api/v1/templates', params: { format: :json, template: template_params.merge(code: 'code2') }
      end.to change(Template, :count).by(1)
    }
  end

  describe 'PUT /update' do
    let!(:template) { create(:template) }

    let!(:template_params) do
      {
        code: 'code',
        title: 'title',
        text: 'text'
      }
    end

    before do
      put "/api/v1/templates/#{template.id}",
          params: { format: :json, template: template_params }
    end

    it { expect(response.successful?).to be true }

    %i[code title text].each do |attr|
      it { expect(response.body).to be_json_eql(template_params[attr].to_json).at_path(attr.to_s) }
    end
  end

  describe 'GET /show' do
    let!(:template) { create(:template) }

    context 'show exist record' do
      before { get "/api/v1/templates/#{template.id}" }

      it { expect(response.successful?).to be true }

      %w[id code title text].each do |attr|
        it { expect(response.body).to be_json_eql(template.send(attr).to_json).at_path(attr) }
      end

      %w[created_at updated_at].each do |attr|
        it { expect(response.body).to be_json_eql(template.send(attr).to_i.to_json).at_path(attr) }
      end
    end

    context 'show not exist record' do
      before { get "/api/v1/templates/#{SecureRandom.uuid}" }

      it { expect(response.successful?).to be false }
      it { expect(response.code).to eq '404' }
      it { expect(response.body).to be_json_eql('record_not_found'.to_json).at_path('error') }
    end
  end

  describe 'DELETE /destroy' do
    let!(:template) { create(:template) }

    it {
      expect do
        delete "/api/v1/templates/#{template.id}"
      end.to change(Template, :count).by(-1)
    }
  end
end
