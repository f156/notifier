# == Schema Information
#
# Table name: notifications
#
#  id            :uuid             not null, primary key
#  way           :integer          not null
#  user_uid      :uuid             not null
#  title         :string
#  text          :text
#  location      :string
#  template_code :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

FactoryBot.define do
  sequence(:location) { |n| "location_#{n}" }

  factory :notification do
    way { :email }
    user_uid { SecureRandom.uuid }
    title
    text
    location
  end
end
