FactoryBot.define do
  factory :profile, class: OpenStruct do
    user do
      {
        id: SecureRandom.uuid,
        email: 'example@gmail.com',
        created_at: Time.current.to_i,
        updated_at: Time.current.to_i,
        is_company: false
      }
    end
    kyc { { review_status: 'completed', verified: true } }
  end
end
