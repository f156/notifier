# == Schema Information
#
# Table name: templates
#
#  id         :uuid             not null, primary key
#  code       :string           not null
#  title      :string           not null
#  text       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  sequence(:code) { |n| "code_#{n}" }
  sequence(:title) { |n| "title_#{n}" }
  sequence(:text) { |n| "text_#{n}" }

  factory :template do
    code
    title
    text
  end
end
