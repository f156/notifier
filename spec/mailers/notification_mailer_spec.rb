require 'rails_helper'

RSpec.describe NotificationMailer, type: :mailer do
  describe 'call' do
    let(:notification) { create(:notification, location: 'test@test.com') }
    let(:mail) { NotificationMailer.call(notification.id) }

    it { expect(mail.subject).to eq(notification.title) }
    it { expect(mail.to).to eq([notification.location]) }
    it { expect(mail.from).to eq([Rails.configuration.info_email]) }
    it { expect(mail.body.encoded).to include(notification.text) }
  end
end
