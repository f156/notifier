# == Schema Information
#
# Table name: notifications
#
#  id            :uuid             not null, primary key
#  way           :integer          not null
#  user_uid      :uuid             not null
#  title         :string
#  text          :text
#  location      :string
#  template_code :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

require 'rails_helper'

RSpec.describe Notification, type: :model do
  it { should validate_presence_of :way }
  it { should validate_presence_of :user_uid }

  it do
    should belong_to(:template)
      .with_primary_key('code')
      .with_foreign_key('template_code')
      .inverse_of(false)
      .optional
  end

  context 'if template_code.nil?' do
    before { allow(subject.template_code).to receive(:nil?).and_return(true) }
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:text) }
  end
end
