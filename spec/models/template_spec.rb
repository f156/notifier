# == Schema Information
#
# Table name: templates
#
#  id         :uuid             not null, primary key
#  code       :string           not null
#  title      :string           not null
#  text       :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'rails_helper'

RSpec.describe Template, type: :model do
  it { should validate_presence_of :code }
  it { should validate_presence_of :title }
  it { should validate_presence_of :text }

  context 'validate uniqueness' do
    subject { FactoryBot.build(:template) }
    it { should validate_uniqueness_of(:code) }
  end
end
